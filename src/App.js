import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";

//View
import Dashboard from './views/Dashboard/Dashboard'

function App() {
    return (
        <Router>
            <Switch>
                <Route path="/">
                    <Dashboard/>
                </Route>
            </Switch>
        </Router>
    );
}

export default App;
