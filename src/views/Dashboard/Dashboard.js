//Components
import TableEventsComponent from "../../components/Events/TableEventsComponent";

function Dashboard() {
    return (
        <div className="container mx-auto">
            <div className="grid grid-cols-1 grid-rows-2  gap-2">
                <div className="bg-white shadow overflow-hidden sm:rounded-lg">
                    <p>Graficas</p>
                </div>
                <div className="bg-white shadow overflow-hidden sm:rounded-lg ">
                    <TableEventsComponent/>
                </div>
            </div>
        </div>
    )
}

export default Dashboard